use crate::proc_macro::TokenStream;
use std::collections::hash_map::{Entry, HashMap};
use syn::{
    parse::{Error, Parse, ParseStream, Result as ParseRes},
    Ident,
    ItemFn,
};

pub fn parser<'stream>(stream: ParseStream<'stream>) -> ParseRes<Inliner> {
    let mut inliner = Inliner::new();
    while !stream.is_empty() {
        let item = ItemFn::parse(stream)?;
        inliner.add(item)?;
    }
    Ok(inliner)
}

pub struct Inliner {
    input: HashMap<Ident, InputEntry>,
    ids: Vec<Ident>,
}

impl Inliner {
    pub fn new() -> Self {
        Self { input: HashMap::new(), ids: Vec::new() }
    }

    pub fn add(&mut self, item: ItemFn) -> ParseRes<()> {
        match self.input.entry(item.ident.clone()) {
            Entry::Occupied(_) => Err(Error::new(
                item.ident.span(),
                format!("Identifier {} already declared", item.ident),
            )),

            Entry::Vacant(entry) => {
                let id = self.ids.len();
                self.ids.push(item.ident.clone());
                entry.insert(InputEntry { id, item });
                Ok(())
            },
        }
    }

    pub fn make_rec(mut self) -> TokenStream {
        unimplemented!()
    }
}

struct InputEntry {
    item: ItemFn,
    id: usize,
}
