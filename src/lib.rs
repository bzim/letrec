extern crate proc_macro;

mod inliner;

use crate::proc_macro::TokenStream;
use syn::parse::Parser;

#[proc_macro]
pub fn letrec(toks: TokenStream) -> TokenStream {
    match inliner::parser.parse(toks) {
        Ok(inliner) => inliner.make_rec(),
        Err(e) => e.to_compile_error().into(),
    }
}
